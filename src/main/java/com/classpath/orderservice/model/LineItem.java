package com.classpath.orderservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name="line_items")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LineItem {

    @Id
    @GeneratedValue(strategy = AUTO)
    private int id;

    @Getter
    @Setter
    private String name;

    @Setter
    @Getter
    private int qty;

    @Setter
    @Getter
    private double unitPrice;

    @ManyToOne
    @JoinColumn(name="order_id")
    @Setter
    @JsonIgnore
    public Order order;

}
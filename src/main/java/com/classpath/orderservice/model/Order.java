package com.classpath.orderservice.model;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.AUTO;

@Data
@EqualsAndHashCode(of = "id")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="orders")
@Slf4j
public class Order {
    @Id
    @GeneratedValue(strategy = AUTO)
    private long id;

    //@Min(value = 2000, message = "Order price should be more than 2k")
   // @Max(value = 20000, message = "Order value should be less than 200K")
    private double totalPrice;

    @NotNull(message = "created date cannot be null")
    private LocalDate createdDt;

    @NotEmpty(message = "customerName cannot be empty")
    private String customerName;

    @OneToMany(mappedBy = "order", fetch = EAGER, cascade = ALL)
    private final Set<LineItem> lineItems = new HashSet<>();

    //scaffolding
    public void addLineItem(LineItem lineItem){
        lineItem.setOrder(this);
        this.lineItems.add(lineItem);
    }
}
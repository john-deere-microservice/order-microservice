package com.classpath.orderservice.config;

import com.classpath.orderservice.model.LineItem;
import com.classpath.orderservice.model.Order;
import com.classpath.orderservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
@Slf4j
public class BootstrapInitializer implements ApplicationListener<ApplicationReadyEvent>{

    private final OrderRepository orderRepository;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        log.info("Application is ready. Bootstrapping the data:: ");

        IntStream.range(1, 10).forEach((index) -> {
            Order order = Order.builder()
                        .customerName("Ramesh - "+index)
                        .createdDt(LocalDate.now())
                        .totalPrice(5000 * index)
                        /*.lineItems(new HashSet<>(Arrays.asList(
                                LineItem.builder().qty(2).name("Macbook-Pro").unitPrice(2_00_000).build(),
                                LineItem.builder().qty(4).name("Macbook-Air").unitPrice(1_80_000).build(),
                                LineItem.builder().qty(4).name("IPad-Pro").unitPrice(80_000).build())))*/
                        .build();
            // set the bidirectional mapping.
            order.addLineItem( LineItem.builder().qty(2).name("Macbook-Pro").unitPrice(2_00_000).build());
            order.addLineItem( LineItem.builder().qty(4).name("Macbook-Air").unitPrice(1_80_000).build());
            order.addLineItem( LineItem.builder().qty(4).name("IPad-Pro").unitPrice(80_000).build());

            log.info("Saving Order data :: {} ", order.toString());
            this.orderRepository.save(order);
        });
    }
}
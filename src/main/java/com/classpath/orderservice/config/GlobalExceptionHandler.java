package com.classpath.orderservice.config;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Error> handleInvalidOrderId(Throwable exception){
      log.error(" Exception caught with invalid order id :: {}", exception.getMessage());
      //new Error(111,23,1, "mesage", "description");
      Error error = Error.builder().errorCode(111).message(exception.getMessage()).build();
      return ResponseEntity.status(NOT_FOUND).body(error);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Error> handleInvalidData(Throwable exception){
      log.error(" Exception caught with invalid order id :: {}", exception.getMessage());
      //new Error(111,23,1, "mesage", "description");
      Error error = Error.builder().errorCode(100).message(exception.getMessage()).build();
      return ResponseEntity.status(BAD_REQUEST).body(error);
    }
}

@RequiredArgsConstructor
@Builder
@Data
class Error {

    private final int errorCode;

    private final String message;
}
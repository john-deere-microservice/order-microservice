package com.classpath.orderservice.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Configuration;

@Configuration
class AppHealthEndpoint implements HealthIndicator {

    @Override
    public Health health() {
        // connect to Kafka broker and verify
        return Health.up().withDetail("Kafka-Service", "Service is UP").build();
    }
}

@Configuration
class PaymentGatewayHealthEnpoint implements HealthIndicator {
    @Override
    public Health health() {
        // connect to Payment Gateway and verify
        return Health.up().withDetail("Payment-Service", "Service is UP").build();
    }
}
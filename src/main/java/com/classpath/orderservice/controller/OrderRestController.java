package com.classpath.orderservice.controller;

import com.classpath.orderservice.model.Order;
import com.classpath.orderservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Set;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
@Slf4j
public class OrderRestController {

    private final OrderService orderService;

    @PostMapping
    @ResponseStatus(CREATED)
    public Order save(@RequestBody @Valid Order order){
        log.info("Came inside the save method :: {}", order);
        return this.orderService.save(order);
    }

    @GetMapping
    @ResponseStatus(OK)
    public Set<Order> fetchAll(){
        return this.orderService.fetchAll();
    }

    @GetMapping("/{id}")
    @ResponseStatus(OK)
    public Order fetchById(@PathVariable long id){
        return this.orderService.fetchById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteById(@PathVariable long id){
        this.orderService.delete(id);
    }
}
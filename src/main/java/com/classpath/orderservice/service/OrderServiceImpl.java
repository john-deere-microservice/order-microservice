package com.classpath.orderservice.service;

import com.classpath.orderservice.model.Order;
import com.classpath.orderservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private final RestTemplate restTemplate;

    @Override
    public Set<Order> fetchAll() {
        return new HashSet(this.orderRepository.findAll());
    }

    @Override
    public Order fetchById(long orderId) {
        return this.orderRepository.findById(orderId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid order id passed"));
    }

    @Override
    public Order save(Order order) {
        log.info(" Came inside the save order method of order service ::::");
        System.out.println(" Came inside the save order method of order service ::::");
        System.out.println(" Came inside the save order method of order service ::::");
        ResponseEntity<Integer> response = this.restTemplate.postForEntity("http://inventory-service:81/api/v1/inventory/1", Integer.class, Integer.class);
        log.info("Hit the Inventory service "+ response.getBody());
        System.out.println("Hit the Inventory service "+ response.getBody());
        System.out.println("Hit the Inventory service "+ response.getBody());
        return this.orderRepository.save(order);
    }

    @Override
    public void delete(long orderId) {
        this.orderRepository.deleteById(orderId);
    }
}
package com.classpath.orderservice.service;

import com.classpath.orderservice.model.Order;

import java.util.Set;

public interface OrderService {

    Set<Order> fetchAll();

    Order fetchById(long orderId);

    Order save(Order order);

    void delete(long orderId);

}